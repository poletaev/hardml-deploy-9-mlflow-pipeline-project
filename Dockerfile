FROM clearlinux/numpy-mp:latest

# RUN swupd update && swupd bundle-add python3-basic

WORKDIR /python-docker

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

#STOPSIGNAL SIGINT

# final configuration
ENV FLASK_APP=app
ARG buildtime_secret=19
ARG threshold_0_1
ARG threshold_1_2 
ARG s3_enpoint
ARG mlflow_uri
ARG s3_id
ARG s3_secret

ENV SECRET_NUMBER=$buildtime_secret
ENV THRESHOLD_0_1=$threshold_0_1
ENV THRESHOLD_1_2=$threshold_1_2
ENV MLFLOW_S3_ENDPOINT_URL=$s3_enpoint
ENV MLFLOW_TRACKING_URI=$mlflow_uri
ENV AWS_ACCESS_KEY_ID=$s3_id
ENV AWS_SECRET_ACCESS_KEY=$s3_secret

RUN echo environ

EXPOSE 8000

ENTRYPOINT ["python3"]
CMD ["app_flask.py"]