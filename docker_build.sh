#!/bin/sh

tag=hw9:0
docker rmi $tag
docker build --platform linux/amd64 --build-arg buildtime_secret=20 \
--build-arg s3_enpoint=http://65.21.49.92:9000 \
--build-arg mlflow_uri=http://65.21.49.92:5000 \
--build-arg s3_id=IAM_ACCESS_KEY \
--build-arg s3_secret=IAM_SECRET_KEY \
-t $tag -t 65.109.8.98:5000/$tag . 

docker push 65.109.8.98:5000/$tag
