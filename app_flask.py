from flask import Flask, jsonify, request, abort
import signal
import sys
from time import sleep
import mlflow

import os

for name, value in os.environ.items():
    print("{0}: {1}".format(name, value))


secret_number = os.environ.get("SECRET_NUMBER")
threshold_0_1 = os.environ.get("THRESHOLD_0_1")
threshold_1_2 = os.environ.get("THRESHOLD_1_2")

# you need to set these env variables in launch.json in VS code
if os.environ.get("MLFLOW_S3_ENDPOINT_URL") is None or \
   os.environ.get("MLFLOW_TRACKING_URI") is None or \
   os.environ.get("AWS_ACCESS_KEY_ID") is None or \
   os.environ.get("AWS_SECRET_ACCESS_KEY") is None:
   print("no credentials for s3 and mlflow!")
   sys.exit(-1)


def handler(sig, frame):
    print(f'YOU CALLED ME WITH {sig}')
    sleep(1)
    print('exit')
    sys.exit(sig)

# signal.signal(signal.SIGKILL, handler) # SIGKILL мы отловить не можем

signal.signal(signal.SIGTERM, handler)
signal.signal(signal.SIGINT, handler)
mlflow.set_tracking_uri("http://65.21.49.92:5000")
client = mlflow.tracking.MlflowClient()
MODEL_PATH = "models:/iris_py/staging"
model = mlflow.pyfunc.load_model(MODEL_PATH)

app = Flask(__name__)


@app.route('/')
def index():
    sleep(1)
    return "<span style='color:red'>Docker works!</span>"


@app.route('/healthcheck')
def check():
    if model is not None:
        return "ok"
    else:
        return "NOT ok"


@app.route("/return_secret_number")
def get_secret_number():
    sleep(1)
    return jsonify({"secret_number": secret_number})

@app.route("/get_source_iris_pred", methods=['GET'])
def iris_pred():
    """
    GET /get_source_iris_pred?sepal_length=<float>&sepal_width=<float>,
    который возвращает {"prediction": <float>} (сделайте модель только по двум признакам);    
    """
    args = request.args
    sepal_length = args.get('sepal_length', type=float, default=0)
    sepal_width = args.get('sepal_width', type=float, default=0)  
    predict = model.predict([sepal_length, sepal_width])[0]
    return jsonify({"prediction": predict})


@app.route("/get_string_iris_pred")
def iris_pred_str():
    """
    GET /get_string_iris_pred?sepal_length=<float>&sepal_width=<float>,
    которая пока возвращает только 501.    
    
    В пайплайне сделайте pyfync модель, чтобы она отдавала словарь: 
    {"class": <float>, "class_str": <setosa|versicolor|virginica>, 
     "threshold_0_1": <float>, "threshold_1_2": <float>}. 

       — threshold_0_1 и threshold_1_2 — коэффициенты, которые получили в п. 2;
       — class — исходное числовое предсказание модели;
       — class_str — значение, которое получается из class путём сравнения с thresholds: 
                          setosa < threshold_0_1 < versicolor < threshold_1_2    
    """
    print(threshold_0_1)
    print(threshold_1_2)
    args = request.args
    sepal_length = args.get('sepal_length', type=float, default=0)
    sepal_width = args.get('sepal_width', type=float, default=0)  
    if not threshold_0_1 or not threshold_1_2:
        return abort(501)
    else:
        predict = model.predict([sepal_length, sepal_width])[0]
        if predict < float(threshold_0_1) if threshold_0_1 else 0:
            predict_str="setosa"
        elif predict < float(threshold_1_2) if threshold_1_2 else 0:
            predict_str="versicolor"
        else:
            predict_str="virginica"
        return jsonify({"class": predict,
                        "class_str": predict_str,
                        "threshold_0_1": float(threshold_0_1),
                        "threshold_1_2": float(threshold_1_2)})



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
